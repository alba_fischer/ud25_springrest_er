DROP table IF EXISTS `salas`;
DROP TABLE IF EXISTS `pelicula`;



CREATE TABLE `pelicula` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `calificacion_edad` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `salas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `pelicula_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`pelicula_id`) REFERENCES `pelicula` (`id`)
);

insert into pelicula (nombre,calificacion_edad)values('Los juegos del hambre',13);
insert into pelicula (nombre,calificacion_edad)values('Ted',16);
insert into pelicula (nombre,calificacion_edad)values('El corredor del laberinto',14);
insert into pelicula (nombre,calificacion_edad)values('Insurgente',12);
insert into pelicula (nombre,calificacion_edad)values('Leal',13);

insert into salas (nombre, pelicula_id) values ('Sala 1',1);
insert into salas (nombre, pelicula_id) values ('Sala 2',2);
insert into salas (nombre, pelicula_id) values ('Sala 3',5);
insert into salas (nombre, pelicula_id) values ('Sala 4',4);