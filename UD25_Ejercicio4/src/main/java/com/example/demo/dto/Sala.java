package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="salas")

public class Sala {
	
	//Atributos de sala
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		@Column(name = "nombre")
		private String nombre;
		
		@ManyToOne
	    @JoinColumn(name="pelicula_id")
	    private Pelicula pelicula_id;

		//Constructores
		/**
		 * 
		 */
		public Sala() {
			
		}

	
		/**
		 * @param id
		 * @param nombre
		 * @param pelicula
		 */
		public Sala(Long id, String nombre, Pelicula pelicula) {
			this.id = id;
			this.nombre = nombre;
			this.pelicula_id = pelicula;
		}


		// getter y setter
		/**
		 * @return the id
		 */
		public Long getId() {
			return id;
		}


		/**
		 * @param id the id to set
		 */
		public void setId(Long id) {
			this.id = id;
		}


		/**
		 * @return the nombre
		 */
		public String getNombre() {
			return nombre;
		}


		/**
		 * @param nombre the nombre to set
		 */
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}


		/**
		 * @return the pelicula
		 */
		public Pelicula getPelicula() {
			return pelicula_id;
		}


		/**
		 * @param pelicula the pelicula to set
		 */
		public void setPelicula(Pelicula pelicula) {
			this.pelicula_id = pelicula;
		}


		//metodo toString
		@Override
		public String toString() {
			return "Sala [id=" + id + ", nombre=" + nombre +", pelicula=" + pelicula_id + "]";
		}
		
}

	

	

	

