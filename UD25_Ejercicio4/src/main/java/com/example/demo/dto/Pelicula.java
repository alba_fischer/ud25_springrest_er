package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="pelicula")
public class Pelicula  {
 
	//Atributos de entidad pelicula
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "calificacion_edad")
	private int calificacion_edad;
	
    @OneToMany
    @JoinColumn(name="id")
    private List<Sala> sala;
	
	//Constructores
	
	public Pelicula() {
	
	}

	/**
	 * @param id
	 * @param nombre
	 * @param calificacion_edad
	 */
	public Pelicula(Long id, String nombre, int calificacion_edad) {
		this.id = id;
		this.nombre = nombre;
		this.calificacion_edad = calificacion_edad;
	}

	
	//Getters y Setters
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @return the calificacion_edad
	 */
	public int getCalificacion_edad() {
		return calificacion_edad;
	}

	/**
	 * @param calificacion_edad the calificacion_edad to set
	 */
	public void setCalificacion_edad(int calificacion_edad) {
		this.calificacion_edad = calificacion_edad;
	}
		
	/**
	 * @return the sala
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Sala")
	public List<Sala> getSala() {
		return sala;
	}

	/**
	 * @param sala the sala to set
	 */
	public void setSala(List<Sala> sala) {
		this.sala = sala;
	}

	//Metodo toString
	@Override
	public String toString() {
		return "Pelicula [id=" + id + ", nombre=" + nombre + ", calificaion edad=" + calificacion_edad + "]";
	}
	
	
	
	
	
}
