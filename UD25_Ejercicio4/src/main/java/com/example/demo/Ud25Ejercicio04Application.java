package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ud25Ejercicio04Application {

	public static void main(String[] args) {
		SpringApplication.run(Ud25Ejercicio04Application.class, args);
	}

}
