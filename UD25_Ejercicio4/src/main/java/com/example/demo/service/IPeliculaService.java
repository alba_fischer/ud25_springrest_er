package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Pelicula;

public interface IPeliculaService {

	//Metodos del CRUD
	public List<Pelicula> listarPeliculas(); //Listar All 
	
	public Pelicula guardarPelicula(Pelicula pelicula);	//Guarda un pelicula CREATE
	
	public Pelicula peliculaXID(Long id); //Leer datos de una pelicula READ
	
	public Pelicula actualizarPelicula(Pelicula pelicula); //Actualiza datos de la pelicula UPDATE
	
	public void eliminarPelicula(Long id);// Elimina la pelicula DELETE
	
	
}