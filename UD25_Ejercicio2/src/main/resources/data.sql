
DROP TABLE IF EXISTS `empleados`;
DROP table IF EXISTS `departamento`;


CREATE TABLE `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `presupuesto` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `apellido` varchar(250) DEFAULT NULL,
  `departamento_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`id`)
);

insert into departamento (nombre,presupuesto)values('Matemáticas',1234);
insert into departamento (nombre,presupuesto)values('Informática',6724);
insert into departamento (nombre,presupuesto)values('Matéria condensada',5845);
insert into departamento (nombre,presupuesto)values('Materiales',4652);
insert into departamento (nombre,presupuesto)values('Química',1542);

insert into empleados (id,nombre, apellido, departamento_id) values (45689572,'Alba','Reverte Gras',1);
insert into empleados (id,nombre, apellido, departamento_id) values (35624158,'Esther','Sancho Lopez',2);
insert into empleados (id,nombre, apellido, departamento_id) values (78954621,'Raquel','Llorca Cartes',5);
insert into empleados (id,nombre, apellido, departamento_id) values (45698752,'Nàdia','Carles Balada',4);

