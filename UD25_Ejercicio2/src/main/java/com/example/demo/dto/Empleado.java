package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="empleados")

public class Empleado {
	
	//Atributos de empleado
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		@Column(name = "nombre")
		private String nombre;
		@Column(name = "apellido")
		private String apellido;
		
		@ManyToOne
	    @JoinColumn(name="departamento_id")
	    private Departamento departamento_id;

		//Constructores
		/**
		 * 
		 */
		public Empleado() {
			
		}

	
		/**
		 * @param id
		 * @param nombre
		 * @param apellido
		 * @param departamento
		 */
		public Empleado(Long id, String nombre, String apellido, Departamento departamento) {
			this.id = id;
			this.nombre = nombre;
			this.apellido = apellido;
			this.departamento_id = departamento;
		}


		// getter y setter
		/**
		 * @return the id
		 */
		public Long getId() {
			return id;
		}


		/**
		 * @param id the id to set
		 */
		public void setId(Long id) {
			this.id = id;
		}


		/**
		 * @return the nombre
		 */
		public String getNombre() {
			return nombre;
		}


		/**
		 * @param nombre the nombre to set
		 */
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}


		/**
		 * @return the apellido
		 */
		public String getApellido() {
			return apellido;
		}


		/**
		 * @param apellido the apellido to set
		 */
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}


		/**
		 * @return the Departamento
		 */
		public Departamento getDepartamento() {
			return departamento_id;
		}


		/**
		 * @param departamento the departamento to set
		 */
		public void setDepartamento(Departamento departamento) {
			this.departamento_id = departamento;
		}


		//metodo toString
		@Override
		public String toString() {
			return "Empleado [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", departamento=" + departamento_id + "]";
		}


}

	

	

	
	