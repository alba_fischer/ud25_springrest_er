DROP TABLE IF EXISTS `articulos`;
DROP table IF EXISTS `fabricante`;


CREATE TABLE `fabricante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `fabricante_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`fabricante_id`) REFERENCES `fabricante` (`id`)
);

insert into fabricante (nombre)values('Sony');
insert into fabricante (nombre)values('Android');
insert into fabricante (nombre)values('Apple');
insert into fabricante (nombre)values('Xiaomi');
insert into fabricante (nombre)values('Samsung');

insert into articulos (nombre, precio, fabricante_id) values ('USB',30,1);
insert into articulos (nombre, precio, fabricante_id) values ('Ordenador',700,3);
insert into articulos (nombre, precio, fabricante_id) values ('Tablet',500,5);
insert into articulos (nombre, precio, fabricante_id) values ('Móbil',400,4);

