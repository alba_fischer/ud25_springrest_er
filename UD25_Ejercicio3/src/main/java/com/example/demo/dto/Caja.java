package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="cajas")

public class Caja {
	
	//Atributos de cajaas
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		@Column(name = "contenido")
		private String contenido;
		@Column(name = "valor")
		private int valor;
		
		@ManyToOne
	    @JoinColumn(name="almacen_id")
	    private Almacen almacen_id;

		//Constructores
		/**
		 * 
		 */
		public Caja() {
			
		}

	
		/**
		 * @param id
		 * @param contenido
		 * @param valor
		 * @param almacen
		 */
		public Caja(Long id, String contenido, int valor, Almacen almacen) {
			this.id = id;
			this.contenido = contenido;
			this.valor = valor;
			this.almacen_id = almacen;
		}


		// getter y setter
		/**
		 * @return the id
		 */
		public Long getId() {
			return id;
		}


		/**
		 * @param id the id to set
		 */
		public void setId(Long id) {
			this.id = id;
		}


		/**
		 * @return the contenido
		 */
		public String getContenido() {
			return contenido;
		}


		/**
		 * @param contenido the contenido to set
		 */
		public void setContenido(String contenido) {
			this.contenido = contenido;
		}


		/**
		 * @return the valor
		 */
		public int getValor() {
			return valor;
		}


		/**
		 * @param valor the valor to set
		 */
		public void setValor(int valor) {
			this.valor = valor;
		}


		/**
		 * @return the Almacen
		 */
		public Almacen getAlmacen() {
			return almacen_id;
		}


		/**
		 * @param almacen the almacen to set
		 */
		public void setAlmacen(Almacen almacen) {
			this.almacen_id = almacen;
		}


		//metodo toString
		@Override
		public String toString() {
			return "Caja [id=" + id + ", contenido=" + contenido + ", valor=" + valor + ", almacen=" + almacen_id + "]";
		}


}

	

	

	
