package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Almacen;
import com.example.demo.service.AlmacenServiceImpl;

@RestController
@RequestMapping("/api")
public class AlmacenController {
	
	@Autowired
	AlmacenServiceImpl almacenServideImpl;
	
	@GetMapping("/almacen")
	public List<Almacen> listarAlmacenes(){
		return almacenServideImpl.listarAlmacenes();
	}
	
	@PostMapping("/almacen")
	public Almacen salvarAlmacen(@RequestBody Almacen almacen) {
		
		return almacenServideImpl.guardarAlmacen(almacen);
	}
	
	@GetMapping("/almacen/{id}")
	public Almacen almacenXID(@PathVariable(name="id") Long id) {
		
		Almacen almacen_xid= new Almacen();
		
		almacen_xid=almacenServideImpl.almacenXID(id);
		
		System.out.println("Almacen XID: " + almacen_xid);
		
		return almacen_xid;
	}
	
	@PutMapping("/almacen/{id}")
	public Almacen actualizarAlmacen(@PathVariable(name="id")Long id,@RequestBody Almacen almacen) {
		
		Almacen almacen_seleccionado= new Almacen();
		Almacen almacen_actualizado= new Almacen();
		
		almacen_seleccionado= almacenServideImpl.almacenXID(id);
		
		almacen_seleccionado.setLugar(almacen.getLugar());
		almacen_seleccionado.setCapacidad(almacen.getCapacidad());
		
		almacen_actualizado = almacenServideImpl.actualizarAlmacen(almacen_seleccionado);
		
		System.out.println("L'almacen actualizado es: "+ almacen_actualizado);
		
		return almacen_actualizado;
	}
	
	@DeleteMapping("/almacen/{id}")
	public void eleiminarAlmacen(@PathVariable(name="id")Long id) {
		almacenServideImpl.eliminarAlmacen(id);
	}
	
	
}

