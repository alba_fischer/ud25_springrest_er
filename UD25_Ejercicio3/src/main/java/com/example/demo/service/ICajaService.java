package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Caja;

public interface ICajaService {

	//Metodos del CRUD
		public List<Caja> listarCajas(); //Listar All 
		
		public Caja guardarCaja(Caja caja);	//Guarda una caja CREATE
		
		public Caja cajaXID(Long id); //Leer datos de una caja READ
		
		public Caja actualizarCaja(Caja caja); //Actualiza datos de la caja UPDATE
		
		public void eliminarCaja(Long id);// Elimina la caja DELETE
}
