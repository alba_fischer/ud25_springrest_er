
DROP TABLE IF EXISTS `cajas`;
DROP table IF EXISTS `almacen`;


CREATE TABLE `almacen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lugar` varchar(100) DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `cajas` (
  `id` char(5) NOT NULL,
  `contenido` varchar(100) DEFAULT NULL,
  `valor` int DEFAULT NULL,
  `almacen_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`almacen_id`) REFERENCES `almacen` (`id`)
);

insert into almacen (lugar,capacidad)values('Barcelona',200);
insert into almacen (lugar,capacidad)values('Madrid',400);
insert into almacen (lugar,capacidad)values('Tarragona',100);
insert into almacen (lugar,capacidad)values('Girona',450);
insert into almacen (lugar,capacidad)values('Lleida',250);

insert into cajas (id,contenido, valor, almacen_id) values (45689,'Ordenadores',50000,1);
insert into cajas (id,contenido, valor, almacen_id) values (35624,'Móbiles',10000,2);
insert into cajas (id,contenido, valor, almacen_id) values (78954,'Muebles',20000,5);
insert into cajas (id,contenido, valor, almacen_id) values (45698,'Comida',1000,4);

